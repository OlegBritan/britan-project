function openPage(evt, pageName) {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(pageName).style.display = "block";
    evt.currentTarget.className += " active";
}



function myFunction() {
    const x = document.getElementById("myDIV");
    const z = document.getElementById("myDIV1");

    const y = document.getElementById("btnAddMore");

    if (x.classList.contains('myDIVHidden')) {
        x.classList.remove('myDIVHidden');
    } else {
        z.classList.remove('myDIVHidden');
        y.style.display = "none";
    }
}


function myFunction1() {
    const x = document.getElementById("myDivA");
    const z = document.getElementById("myDivB");
    const a = document.getElementById("myDivC");
    const b = document.getElementById("myDivD");
    const c = document.getElementById("myDivE");
    const d = document.getElementById("myDivF");

    const y = document.getElementById("btnAddMore1");

    if (x.classList.contains('myDIVHidden') && c.classList.contains('myDIVHidden') && a.classList.contains('myDIVHidden')) {
        x.classList.remove('myDIVHidden');
        c.classList.remove('myDIVHidden');
        a.classList.remove('myDIVHidden');
    } else {
        z.classList.remove('myDIVHidden');
        b.classList.remove('myDIVHidden');
        d.classList.remove('myDIVHidden');
        y.style.display = "none";
    }
}