const tabArray = document.querySelectorAll('.tabs-title');
const textArray = document.querySelectorAll('.tabs-text');

tabArray.forEach(function (tab,index) {
    tab.dataset.num = index+1;
});
textArray.forEach(function (tab,index) {
    tab.dataset.num = index+1;
});

document.body.onkeydown = function(event) {
    if(event.key ==='Tab'){
        event.preventDefault();
        const currentTab = document.querySelector('.tabs-title.active');
        let currentNam = currentTab.getAttribute('data-num');
        console.log(currentNam);
        if (currentNam >= tabArray.length) {
            tabArray[tabArray.length-1].classList.remove('active');
            tabArray[0].classList.add('active');
            textArray[0].classList.add('active')
        }else {
            currentTab.classList.remove('active');
            currentTab.nextElementSibling.classList.add('active');
            textArray[currentNam - 1].classList.remove('active');
            textArray[currentNam].classList.add('active')
        }
    }
};

