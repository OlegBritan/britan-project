const wrapper = document.createElement('div');
wrapper.style.display = 'flex';
wrapper.id = 'wrapper';
document.body.appendChild(wrapper);


const input =  document.createElement("input");
input.classList.add('price');
input.placeholder = 'price';
document.body.querySelector('#wrapper').appendChild(input);
input.style.outlineStyle = 'none';
input.style.border = "2px solid gray";
input.style.fontSize='20px';


const text = document.createElement("p");
text.innerText = 'Price $';
document.body.querySelector('#wrapper').insertBefore(text, input);



const spanWrapper = document.createElement('div');
const span = document.createElement("span");
const button = document.createElement("button");
document.body.insertBefore(spanWrapper, document.body.querySelector('#wrapper'));
button.innerText = 'X';
spanWrapper.appendChild(span);
spanWrapper.appendChild(button);
spanWrapper.style.opacity = '0';


const text2 = document.createElement("p");
text2.innerText = 'Please enter correct price';
document.body.appendChild(text2);
text2.style.opacity = '0';

input.onfocus = function() {
    input.style.border = "2px solid green"
};


input.onblur = function() {
    input.style.border = "2px solid gray";
    if (input.value && !isNaN(+input.value)) {
        spanWrapper.style.opacity = '1';
        span.innerText =`Price is: ${input.value}$`;
        input.style.color = 'green';
        text2.style.opacity = '0';
    }else{
        text2.style.opacity = '1';
        input.style.border = "2px solid red";
        spanWrapper.style.opacity = '0';
    }
};
button.onclick = function () {
    spanWrapper.style.opacity = '0';
    input.value = '';
};
