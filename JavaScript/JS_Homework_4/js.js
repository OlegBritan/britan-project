let firstName = prompt('Enter your name:');
let lastName = prompt('Enter your surname:');
let student = createNewUser(firstName, lastName);
function createNewUser(firstName, lastName) {
    let newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        enumerable: true,
        configurable: true,
        value: firstName
    });
    Object.defineProperty(newUser, 'lastName', {
        enumerable: true,
        configurable: true,
        value: lastName
    });
    Object.defineProperty(newUser, 'getLogin', {
        enumerable: true,
        value: function () {
            return `${
                this.firstName
                    .charAt(0)
                    .toLowerCase()}${
                this.lastName
                    .toLowerCase()
                }`;
        }
    });
    return newUser;
}
console.log(student.firstName);
console.log(student.lastName);
console.log(student.getLogin());