let firstName = prompt('Enter your name:');
let lastName = prompt('Enter your surname:');
let birthday = prompt('Enter your birthday:', 'dd.mm.yyyy').split('.');
let date = new Date(birthday[2], birthday[1]-1, birthday[0]);
let student = createNewUser(firstName, lastName, date);

function createNewUser(firstName, lastName, birth) {
    let newUser = {};
    Object.defineProperty(newUser, 'birthday', {
        enumerable: true,
        configurable: true,
        value: birth
    });
    Object.defineProperty(newUser, 'firstName', {
        enumerable: true,
        configurable: true,
        value: firstName
    });
    Object.defineProperty(newUser, 'lastName', {
        enumerable: true,
        configurable: true,
        value: lastName
    });
    Object.defineProperty(newUser, 'getLogin', {
        enumerable: true,
        value: function () {
            return `${
                this.firstName
                    .charAt(0)
                    .toLowerCase()}${
                this.lastName
                    .toLowerCase()
                }`;
        }
    });
    Object.defineProperty(newUser, 'getAge', {
        enumerable: true,
        value: function () {
            const today = new Date();
            if(today.getMonth()>=this.birthday.getMonth()){
                if(today.getDay()>=this.birthday.getDay()){
                    return today.getFullYear()-this.birthday.getFullYear();
                }else{
                    return today.getFullYear()-this.birthday.getFullYear()-1;
                }
            }else{
                return today.getFullYear()-this.birthday.getFullYear()-1;
            }
        }
    });
    Object.defineProperty(newUser, 'getPassword', {
        enumerable: true,
        value: function () {
            return `${
                this.firstName
                    .charAt(0)
                    .toUpperCase()}${
                this.lastName
                    .toLowerCase()}${
                this.birthday.getFullYear()
                }`;
        }
    });
    return newUser;
}
// console.log(student.firstName);
// console.log(student.lastName);
// console.log(student.birthday);
// console.log(student.getLogin());
console.log(student.getAge());
console.log(student.getPassword());