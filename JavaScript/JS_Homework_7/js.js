function createElements(list) {
    return Array.isArray(list) && list
        .map(function (item) {
            return '<li>${item}</li>';

        })
        .join('') || '';
}