const input = document.getElementById('inputPassword');
const inputRepeat = document.getElementById('inputPasswordRepeat');
const btn = document.getElementsByClassName('btn')[0];
const p = document.getElementById('error');
p.style.opacity='0';
p.style.color='red';

document.body.addEventListener('click', function (event) {
    if (event.target.classList.contains('fa-eye')) {
        event.target.classList.toggle('fa-eye-slash');
        if (event.target.previousElementSibling.type === 'text') {

            event.target.previousElementSibling.type = 'password'
        } else {
            event.target.previousElementSibling.type = 'text'
        }
    }
    if (event.target.classList.contains('btn')) {
        event.preventDefault();
        if (input.value === inputRepeat.value) {
            p.style.opacity='0';
            alert('You are welcome');

        }else {
            p.style.opacity='1';

        }


    }

});
